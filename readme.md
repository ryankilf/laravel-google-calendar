# What this is
This is a sample Laravel site which allows you to show google calendar events on your site.

## What this isn't (yet)
* This is NOT a nice package which you can integrate into an existing Laravel site. 
* Also, it probably shouldn't use twig if it's going to be a laravel package...
* There should have been SOME attempt to style it in some way as well

## Getting Started
* Download the project
* If you like you can use the vagrant file to create a virtual machine which you can access from events.dev

### The Vagrant file (Optional)
* Run Vagrant Up
    * The Vagrant file was created using the [tomorrow lab vagrant builder](https://bitbucket.org/thetomorrowlab/vagrant-builder), so you will need to have the prerequisites descrbed there
* SSH into the vagrant box and cd into /vagrant/site

### Next
* Assuming you have a shell window open at the root of the laravel installation (this is NOT the root of the repository, it's [REPOSITORY_ROOT]/site)
    * Run Composer Update
    * Set up your .env file as you normally would.


### Google API Console
* Sign into the google API console and set up a Service account.
* Add the email address from the service account to every calendar you want to share
* In the google API console you'll need to save a json file that's associated with the service account
* Put it somewhere *OFF THE DOCUMENT ROOT* (I have mine in a (not committed) directory at /vagrant/off-root-files on my dev machine)
* Update your .env file with the following entries
    * *GOOGLE_CALENDAR_KEY_LOCATION* (full path to the json file
    * *CALENDAR_YEARS_BACK* number of years in the past that you want to sync events from
    * *CALENDAR_YEARS_FORWARD* number of years forward that you want to sync events from

### Artisan Commands
* To get your events you'll need to run two artisan commands. One to get your calendars, and one to get the events in each calendar
    * run `php artisan googlecalendar:updatecalendars` to get your calendars
    * run `php artisan googlecalendar:updateevents` to get the events 
* If everything's gone well (and the commands are fairly verbose, so you should see a lot of good stuff if it has) you should be able to go to the homepage of your site and see calendar events.