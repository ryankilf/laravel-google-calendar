#!/usr/bin/env bash
if [[ ! -f "/vagrant/site/composer.json" ]]; then

    cd /vagrant
    mkdir site
    sudo git clone https://github.com/roots/bedrock.git site
    cd /vagrant/site
    composer install
    cp /vagrant/helper-scripts/wordpress-cli.config.yaml /vagrant/wp-cli.local.yml
    sed "s/{{DOMAIN}}/$1/g"  /vagrant/helper-scripts/wordpress.env > /vagrant/site/.env
fi;
