#!/usr/bin/env bash

cd ~
wget http://xdebug.org/files/xdebug-2.4.0.tgz
tar -xvzf xdebug-2.4.0.tgz
cd xdebug-2.4.0
sudo apt-get install php7.0-dev -y
phpize
./configure
make
cp modules/xdebug.so /usr/lib/php/20151012
