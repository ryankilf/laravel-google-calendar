#!/usr/bin/env bash
if [[ ! -f "/vagrant/site/composer.json" ]]; then

    sudo composer global require "laravel/installer=~1.1"
    sudo composer global dump-autoload
    cd /vagrant
    sudo bash -c 'sudo ~/.composer/vendor/bin/laravel new site; bash'

fi;

if [[ ! -d "/vagrant/site/vendor" ]]; then
    cd /vagrant/site
    sudo bash -c 'sudo composer install'
    wget https://raw.githubusercontent.com/laravel/framework/5.2/.editorconfig
    sed "s/{{DOMAIN}}/$1/g"  /vagrant/helper-scripts/laravel.env > /vagrant/site/.env
    php artisan key:generate
fi;