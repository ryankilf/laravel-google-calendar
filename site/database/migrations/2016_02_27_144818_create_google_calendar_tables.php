<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoogleCalendarTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('google_calendars', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('id');
        $table->boolean('enabled')->default(true);
        $table->char('foreground_colour', 40);
        $table->char('background_colour', 40);
        $table->char('etag', 255);
        $table->char('calendar_uid', 255);
        $table->text('summary', 512);
        $table->char('description', 255);
        $table->dateTime('synced_at')->nullable();
        $table->char('slug', 255);
        $table->softDeletes();
        $table->timestamps();
      });

      Schema::create('google_calendar_events', function ($table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->integer('calendar_id')->unsigned();
          $table->char('event_uid', 255)->unique();
          $table->char('ical_uid', 255);
          $table->char('summary', 255);
          $table->text('description');
          $table->text('location');
          $table->boolean('all_day_event')->default(false);
          $table->dateTime('start_date');
          $table->dateTime('end_date');
          $table->char('slug', 255);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('google_calendar_calendar');
      Schema::drop('google_calendar_events');
    }
}
