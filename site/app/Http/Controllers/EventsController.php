<?php
/**
 * Created by PhpStorm.
 * User: ryan
 * Date: 27/02/2016
 * Time: 16:06
 */

namespace App\Http\Controllers;


use App\Calendar;
use App\CalendarEvent;
use Illuminate\Http\Request;
use \DateTime;
use Illuminate\Support\Facades\Input;

class EventsController extends Controller
{

    const AGENDA_INTERVAL = 'P8D';

    private function addSearchFormDetails(&$pageDetails)
    {
        $pageDetails['searchCalendars'] = Calendar::get();
    }

    private function displayEventsList($prefs = [])
    {

        $defaultPrefs = [
            'calendars' => [],
            'numbered_paging' => false,
            'overrideTitle' => '',
            'reverseOrder' => false
        ];

        $prefs = array_merge($defaultPrefs, $prefs);

        $pageDetails = [];
        if ($prefs['start_date'] instanceof \DateTimeInterface) {
            $pageDetails['title'] = 'Events between ' . $prefs['start_date']->format('d/m/Y') . ' - ' . $prefs['end_date']->format('d/m/Y');
        }
        $calendarIds = [];

        if (count($prefs['calendars']) > 0) {
            $calendarNames = [];

            foreach ($prefs['calendars'] as $calendar) {
                $calendarIds[] = $calendar->id;
                $calendarNames[] = $calendar->summary;
            }
            $pageDetails['title'] .= ' in ' . join(', ', $calendarNames);
        }

        if ($prefs['overrideTitle'] != '') {
            $pageDetails['title'] = $prefs['overrideTitle'];
        }

        if (!array_key_exists('events', $prefs) || !$prefs['events'] instanceof \ArrayAccess) {
            $events = $this->getEventsList($prefs, $calendarIds);
        } else {
            $events = $prefs['events'];
        }

        $eventsInDays = $this->groupEventsIntoDays($prefs, $events);

        $compareDate = clone $prefs['start_date'];
        $compareDate->add(new \DateInterval('P60D'));

        if ($compareDate < $prefs['end_date']) {

            $eventsInDays = array_filter($eventsInDays,
                function ($day) {
                    return !empty($day['events']);
                }
            );
        }

        if ($prefs['reverseOrder']) {
            $eventsInDays = array_reverse($eventsInDays);
        }

        $pageDetails['eventsInDays'] = $eventsInDays;
        $pageDetails['events'] = $events;


        if ($prefs['numbered_paging']) {
            $events->appends(Input::except('page'));
            $pageDetails['numbered_paging'] = true;
        } else {
            $monthlyPaging = [];
            if (count($events) > 0) {
                $thisMonth = new \DateTimeImmutable($events[0]->start_date);
                $thisMonth = $thisMonth->setDate($thisMonth->format('Y'), $thisMonth->format('m'), 15);
                $previousMonth = $thisMonth->sub(new \DateInterval('P1M'));
                $nextMonth = $thisMonth->add(new \DateInterval('P1M'));
            }

            $monthlyPagingCalendar = null;
            if(count($prefs['calendars']) == 1) {
                $monthlyPagingCalendar = $prefs['calendars'][0];
            }

            if (CalendarEvent::hasEventsInMonth($previousMonth, $monthlyPagingCalendar)) {
                $monthlyPaging['previous'] = ['date' => $previousMonth, 'calendar' => $monthlyPagingCalendar];
            }

            if (CalendarEvent::hasEventsInMonth($thisMonth, $monthlyPagingCalendar)) {
                $monthlyPaging['whole_month'] = ['date' => $thisMonth, 'calendar' => $monthlyPagingCalendar];
            }

            if (CalendarEvent::hasEventsInMonth($nextMonth, $monthlyPagingCalendar)) {
                $monthlyPaging['next_month'] = ['date' => $nextMonth, 'calendar' => $monthlyPagingCalendar];
            }
            $pageDetails['monthlyPaging'] = $monthlyPaging;

        }


        $this->addSearchFormDetails($pageDetails);

        $content = view('events/events-list', $pageDetails)->render();
        echo $content;
    }


    public function displayAgenda()
    {

        $startDate = new \DateTime('today');
        $endDate = new \DateTime();
        $endDate->add(new \DateInterval(self::AGENDA_INTERVAL));

        return $this->displayEventsList(['start_date' => $startDate, 'end_date' => $endDate]);
    }

    public function displayMonths($year, Calendar $calendar = null)
    {
        $months = CalendarEvent::getMonthsInYear($year, $calendar);

        if (count($months) == 0) {
            abort(404, 'Page not found');
        }

        $pageDetails = [];
        $pageDetails['title'] = 'Events in ' . $year;
        $pageDetails['monthsInYear'] = $months;
        $pageDetails['year'] = $year;
        $content = view('events/months-list', $pageDetails)->render();
        echo $content;

    }


    private function getEventsForDate(\DateTime $oneDate, $events)
    {
        $returnedEvents = [];
        $oneDateFormattedString = $oneDate->format('Y-m-d');

        foreach ($events as $event) {
            $eventStartDate = new \DateTime($event->start_date);
            if ($eventStartDate->format('Y-m-d') == $oneDateFormattedString) {
                $returnedEvents[] = $event;
            }
        }
        return $returnedEvents;
    }

    public function displayEventsInMonth($year, $month)
    {
        $startDate = new \DateTime($year . '-' . $month . '-01 12:00');
        $endDate = clone $startDate;
        $endDate->add(new \DateInterval('P' . $startDate->format('t') . 'D'));
        return $this->displayEventsList(['start_date' => $startDate, 'end_date' => $endDate]);
    }

    public function displayCalendars()
    {
        $calendars = Calendar::get();

        $pageDetails = [];
        $pageDetails['title'] = 'All Calendars ';
        $pageDetails['calendars'] = $calendars;
        $content = view('events/calendars-list', $pageDetails)->render();
        echo $content;
    }

    public function displayOneCalendar($calendarSlug)
    {
        try {
            $calendar = Calendar::where('slug', '=', $calendarSlug)->firstOrFail();
        } catch (\Exception $e) {
            abort(404);
        }

        $startDate = new \DateTime('today');
        $endDate = new \DateTime();
        $endDate->add(new \DateInterval(self::AGENDA_INTERVAL));

        $this->displayEventsList(['start_date' => $startDate, 'end_date' => $endDate, 'calendars' => [$calendar]]);

    }

    public function displayMonthsInOneCalendarYear($calendarSlug, $year)
    {
        try {
            $calendar = Calendar::where('slug', '=', $calendarSlug)->firstOrFail();
        } catch (\Exception $e) {
            abort(404);
        }
        $this->displayMonths($year, $calendar);
    }

    public function displayEventsInMonthForCalendar($calendarSlug, $year, $month)
    {
        try {
            $calendar = Calendar::where('slug', '=', $calendarSlug)->firstOrFail();
        } catch (\Exception $e) {
            abort(404);
        }

        $startDate = new \DateTime($year . '-' . $month . '-01 12:00');
        $endDate = clone $startDate;
        $endDate->add(new \DateInterval('P' . $startDate->format('t') . 'D'));
        $this->displayEventsList(['start_date' => $startDate, 'end_date' => $endDate, 'calendars' => [$calendar]]);

    }

    public function displayEvent($eventSlug)
    {
        try {
            $event = CalendarEvent::where('slug', '=', $eventSlug)->firstOrFail();
        } catch (\Exception $e) {
            abort(404);
        }

        $pageDetails = [];
        $pageDetails['title'] = $event->summary;
        $pageDetails['event'] = $event;
        echo view('events/one-event', $pageDetails)->render();

    }

    public function performSearch(Request $request)
    {

        $calendarIds = [];
        if (is_array($request->query('search_calendar'))) {
            foreach ($request->query('search_calendar') as $oneCalendar) {
                $calendarIds[] = intval($oneCalendar);
            }
        }

        $query = CalendarEvent::select('*');

        if (count($calendarIds) > 0) {
            $query->whereIn('calendar_id', $calendarIds);
        }

        if ($request->query('event_name') != '') {
            $query->whereRaw('match (summary) against (?)', [$request->query('event_name')]);
        }

        $now = new DateTime();
        $reverseOrder = false;

        if ($request->query('past_events') == '1') {
            $query->where('end_date', '<', $now);
            $query->orderBy('start_date', 'DESC');
            $reverseOrder = true;

        } else {
            $query->where('end_date', '>=', $now);
            $query->orderBy('start_date', 'ASC');

        }

        $results = $query->paginate(20);

        $eventParams = [
            'events' => $results,
            'overrideTitle' => 'Search Results',
            'reverseOrder' => $reverseOrder,
            'numbered_paging' => true
        ];

        if (count($results) > 0) {
            if ($reverseOrder) {
                $eventParams['start_date'] = new \DateTime($results->last()->start_date);
                $eventParams['end_date'] =  new \DateTime($results->first()->start_date);
            } else {
                $eventParams['start_date'] =  new \DateTime($results->first()->start_date);
                $eventParams['end_date'] =  new \DateTime($results->last()->start_date);
            }
            $this->displayEventsList($eventParams);
        } else {
            $this->displayNoSearchResultsPage();
        }
    }

    private function getEventsList($prefs = [], $calendarIds = [])
    {
        $eventQuery = CalendarEvent::where('start_date', '>', $prefs['start_date']);

        if (count($calendarIds) > 0) {
            $eventQuery->whereIn('calendar_id', $calendarIds);
        }

        $events = $eventQuery->where('start_date', '<=', $prefs['end_date'])
            ->orderBy('start_date', '>')
            ->get();
        return $events;
    }

    private function groupEventsIntoDays($prefs, $events)
    {
        $daysToFill =
            new \DatePeriod($prefs['start_date'], new \DateInterval('P1D'), $prefs['end_date']);

        $eventsInDays = [];
        if ($prefs['start_date'] == $prefs['end_date']) {
            $eventsInDays[] = [
                'date' => $prefs['start_date'],
                'events' => $this->getEventsForDate($prefs['start_date'], $events)
            ];
        } else {
            foreach ($daysToFill as $oneDate) {
                $eventsInDays[] = [
                    'date' => $oneDate,
                    'events' => $this->getEventsForDate($oneDate, $events)
                ];
            }
        }
        return $eventsInDays;
    }

    private function displayNoSearchResultsPage()
    {

    }
}