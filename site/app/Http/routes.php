<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    $controller = new \App\Http\Controllers\EventsController();
    $controller->displayAgenda();
});


Route::get('/search', function (\Illuminate\Http\Request $request) {
    $controller = new \App\Http\Controllers\EventsController();
    $controller->performSearch($request);
});

Route::get('{year}', function ($year) {
    $controller = new \App\Http\Controllers\EventsController();
    $controller->displayMonths($year);
})->where('year', '[0-9]+');

Route::get('{year}/{month}', function ($year, $month) {
    $controller = new \App\Http\Controllers\EventsController();
    $controller->displayEventsInMonth($year, $month);
})->where(['year' => '[0-9]+', 'month' => '[0-9]+']);

Route::get('/calendars', function () {
    $controller = new \App\Http\Controllers\EventsController();
    $controller->displayCalendars();
});

Route::get('/calendars/{calendarSlug}', function ($calendarSlug) {
    $controller = new \App\Http\Controllers\EventsController();
    $controller->displayOneCalendar($calendarSlug);
});

Route::get('/calendars/{calendarSlug}/{year}', function ($calendarSlug, $year) {
    $controller = new \App\Http\Controllers\EventsController();
    $controller->displayMonthsInOneCalendarYear($calendarSlug, $year);
})->where('year', '[0-9]+');

Route::get('/calendars/{calendarSlug}/{year}/{month}', function ($calendarSlug, $year, $month) {
    $controller = new \App\Http\Controllers\EventsController();
    $controller->displayEventsInMonthForCalendar($calendarSlug, $year, $month);
})->where(['year' => '[0-9]+', 'month' => '[0-9]+']);


//This should be last as if it's before the year end point it will catch it.
//And we can't really use the ->where for this easily
Route::get('/{eventSlug}', function ($eventSlug) {
    $controller = new \App\Http\Controllers\EventsController();
    $controller->displayEvent($eventSlug);
});