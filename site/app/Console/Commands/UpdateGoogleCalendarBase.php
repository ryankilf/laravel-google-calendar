<?php
namespace App\Console\Commands;

use Google_Auth_AssertionCredentials;
use Google_CalendarService;
use Google_Client;
use Google_Http_Batch;
use Google_Service_Calendar;
use Google_Service_Calendar_CalendarList;
use Illuminate\Console\Command;

class UpdateGoogleCalendarBase extends Command
{

    public $settings;
    public $client;
    public $service;
    public $batch;

    public function __construct()
    {
        parent::__construct();
        $this->setupClient();
    }


    protected function setupClient()
    {

        if (!getenv("GOOGLE_CALENDAR_KEY_LOCATION")) {
            throw new \Exception('Google Analytics API private key is not uploaded. Please configure Google Analytics access on the System / Settings / Google Analytics page.');
        }
        $this->client = new Google_Client();
        //$this->client->setApplicationName("deft-racer-89621");

        $this->client = new Google_Client();
        //$this->client->setApplicationName("deft-racer-89621");
        $this->client->useApplicationDefaultCredentials();
        $this->client->setAuthConfig(getenv("GOOGLE_CALENDAR_KEY_LOCATION"));
        $this->client->setScopes(['https://www.googleapis.com/auth/calendar.readonly']);


        //$this->client->setClientId($this->settings->client_id);

        $this->batch = new Google_Http_Batch($this->client);
        $this->service = new Google_Service_Calendar($this->client);
    }
}