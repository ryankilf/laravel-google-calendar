<?php namespace App\Console\Commands;

use App\Calendar;
use App\CalendarEvent;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UpdateEvents extends UpdateGoogleCalendarBase
{
    protected $name = 'googlecalendar:updateevents';
    protected $description = 'Updates Calendar events';
    const DEFAULT_YEARS_FORWARD = 5;
    const DEFAULT_YEARS_BACK = 5;



    public function fire()
    {
        $startTime = microtime(true);

        $years_forward = intval(getenv('CALENDAR_YEARS_FORWARD'));
        if (!is_numeric($years_forward) || $years_forward <= 0) {
            $years_forward = self::DEFAULT_YEARS_FORWARD;
        }
        $years_back = intval(getenv('CALENDAR_YEARS_BACK'));
        if (!is_numeric($years_back) || $years_back <= 0) {
            $years_back = self::DEFAULT_YEARS_BACK;
        }

        $timeMax = new \DateTime('+' . $years_forward . ' years');
        $timeMin = new \DateTime('-' . $years_back . ' years');

        if (getenv('DELETE_OLD_EVENTS') == 'Yes') {
            $this->info('N.B. At the end of this script, all events with a start date before ' . $timeMin->format('Y-m-d H:i:s') . ' will be HARD deleted');
            $this->comment('They will still exist on your google calendar, but they will not be on the website any more');
            $this->comment('If that\'s not something you want, press Ctrl+C *now*');
        }

        //shuffle sometimes in case we get a crash in one calendar somehow..
        $calendars = Calendar::where('enabled', '=', 1)->orderBy('synced_at', 'ASC')->get();
        if (rand(1, 8) == 2) {
            $calendars->shuffle();
        }

        foreach ($calendars as $calendar) {
            $calendarStartProcessingTime = microtime(true);
        $this->info('processing ' . $calendar->calendar_uid);
            /**
             * Full sync will occasionally happen on the calendars, just in case...
             */
            $newSyncDate = new \DateTime('now');
            $calendarUpdatedOk = $this->updateEventsForCalendar($calendar, $timeMin, $timeMax, rand(1, 10) == 4);
            if($calendarUpdatedOk) {
                $calendar->synced_at = $newSyncDate; //Assign this to the calendar AFTER the syncing
                $calendar->save();
            }
            $calendarEndProcessingTime = microtime(true);
            $this->displayTimeDiff('Time to sync calendar', $calendarStartProcessingTime, $calendarEndProcessingTime);
        }

        if (getenv('DELETE_OLD_EVENTS') == 'Yes') {
            $this->info('Deleting events before ' . $timeMin->format('Y-m-d H:i:s'));
            CalendarEvent::where('start_date', '<', $timeMin)->delete();
        }
        $this->displayTimeDiff('Time to sync all calendars', $startTime, microtime(true));
    }

    function displayTimeDiff($message, $start, $end)
    {
        $this->info($message.' : '.abs($start - $end));
    }

    /**
     *
     * @param Calendar $calendar
     * @param \DateTime $timeMin
     * @param \DateTime $timeMax
     * @param bool|false $fullSync
     */
    private function updateEventsForCalendar(Calendar $calendar, \DateTime $timeMin, \DateTime $timeMax, $fullSync = false)
    {
        $this->info('Making Calendar request for '.$calendar->name);

        $params = [
            'singleEvents' => true,
            'showDeleted' => true,
            'timeMax' => $timeMax->format(\DateTime::RFC3339),
            'timeMin' => $timeMin->format(\DateTime::RFC3339),
        ];

        if($calendar->synced_at && !$fullSync) {
            $originalSyncDate = new \DateTime($calendar->synced_at);
            $params['updatedMin'] = $originalSyncDate->format(\DateTime::RFC3339);
            $this->info('Making Calendar request for events modified since ' .$originalSyncDate->format(\DateTime::RFC3339));
        } else {
            $this->info('Making first ever request with calendar');
        }

        $pageNumber = 1;

        while (true) {
            try {
                $eventList = $this->service->events->listEvents($calendar->calendar_uid, $params);
            } catch (\Exception $e) {

                $this->error('Failed to get events for this calendar');
                $this->info('Failed on page '. $pageNumber);

                if($pageNumber == 1) {
                    $this->info('This is likely to be because the service account user does not have access to this calendar');
                    $this->info('You may ignore this if that\'s what you want');
                }
                return false;
            }
            $pageNumber ++;

            foreach ($eventList->getItems() as $event) {
                $this->processOneEvent($event, $calendar);
            }
            $pageToken = $eventList->getNextPageToken();
            if (!$pageToken) {
                return true;
            }
            $this->info('Making request with refresh token');
            $params['pageToken'] = $pageToken;
        }
    }

    private function processOneEvent(\Google_Service_Calendar_Event $googleServiceCalendarEvent, Calendar $calendar)
    {
        $allDay = false;
        if ($googleServiceCalendarEvent->getStart()->getDateTime()) {
            $startTime = new \DateTime($googleServiceCalendarEvent->getStart()->getDateTime());
            $endTime = new \DateTime($googleServiceCalendarEvent->getEnd()->getDateTime());

        } else {
            $startTime = new \DateTime($googleServiceCalendarEvent->getStart()->getDate());
            $endTime = new \DateTime($googleServiceCalendarEvent->getEnd()->getDate());
            $allDay = true;
        }


        $uid = $googleServiceCalendarEvent->getId();


        $this->comment('Event found - ' . $googleServiceCalendarEvent->getSummary() . ' - on ' . $startTime->format('Y-m-d H:i') . ' - ' . $googleServiceCalendarEvent->getId());
        try {
            $event = CalendarEvent::where('event_uid', $googleServiceCalendarEvent->getId())->firstOrFail();

        } catch (ModelNotFoundException $e) {
            if ($googleServiceCalendarEvent->getStatus() == 'cancelled') {
                return;
            }
            $this->comment('New Event!');
            $event = new CalendarEvent();
        }

        if ($googleServiceCalendarEvent->getStatus() == 'cancelled') {
            $event->delete();
            return;
        }

        $event->calendar_id = $calendar->id;
        $event->event_uid = $uid;
        $event->ical_uid = $googleServiceCalendarEvent->getICalUID();
        $event->start_date = $startTime;
        $event->all_day_event = $allDay;
        $event->end_date = $endTime;
        $event->summary = (string)$googleServiceCalendarEvent->getSummary();
        $event->description = (string)$googleServiceCalendarEvent->getDescription();
        $event->location = (string)$googleServiceCalendarEvent->getLocation();
   /*     if(!$event->image) {
            $this->attemptToAddImageToEvent($event, $calendar);
        } */
        $event->save();
    }

    private function attemptToAddImageToEvent(GoogleCalendarEvent $event, GoogleCalendar $calendar)
    {

    }
}