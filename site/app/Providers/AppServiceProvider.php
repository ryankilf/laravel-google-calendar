<?php

namespace App\Providers;

use App\Calendar;
use App\CalendarEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Calendar::creating(function ($calendar) {
            $this->setSlugFromField($calendar, 'slug', 'summary');
        });

        CalendarEvent::creating(function ($event) {
            $value = $event->summary;

            if (is_a($event->start_date, \DateTime::class)) {
                $value .= ' '.$event->start_date->format('Y-m-d');
            };

            $this->setSlugFromValue($event, 'slug', $value);
        });
    }

    private function setSlugFromField(Model $model, $slugFieldName, $basedOnField) {

        $this->setSlugFromValue($model, $slugFieldName, $model->$basedOnField);
    }

    private function setSlugFromValue(Model $model, $slugFieldName, $slugValue) {
        $slugValue = Str::slug($slugValue);

        $i = '';
        do {
            $finalSlug = $slugValue.$i;
            $i --;
        } while ($model->where($slugFieldName, $finalSlug)->count() > 0);
        $model->$slugFieldName = $finalSlug;
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
