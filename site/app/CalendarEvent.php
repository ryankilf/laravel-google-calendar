<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CalendarEvent extends Model
{
    public $table = 'google_calendar_events';

    public static function hasEventsInMonth(\DateTimeInterface $dateTime, $calendar = null)
    {
        $eventsBuilder = DB::table('google_calendar_events')
            ->select(DB::raw('MONTH(`start_date`) AS start_month'))
            ->whereRaw('YEAR(`start_date`) = ' . intval($dateTime->format('Y')))
            ->whereRaw('MONTH(`start_date`) = ' . intval($dateTime->format('n')));

        if ($calendar != null) {
            $eventsBuilder->where('calendar_id', '=', $calendar->id);
        }


        $numberOfEvents = $eventsBuilder->count();

        return ($numberOfEvents > 0);
    }

    public function getSummaryAndDate()
    {
        $summaryAndDate = $this->summary;

        if ($this->start_date instanceof \DateTime) {
            $summaryAndDate .= ' ' . $this->start_date->format('Y m d');
        }
        return $summaryAndDate;
    }

    public static function getMonthsInYear($year, Calendar $calendar = null)
    {
        $monthsInYearQuery = DB::table('google_calendar_events')
            ->select(DB::raw('MONTH(`start_date`) AS start_month'))
            ->whereRaw('YEAR(`start_date`) = ' . intval($year));

        if ($calendar instanceof Calendar) {
            $monthsInYearQuery->where('calendar_id', '=', $calendar->id);
        }


        $monthsInYearQuery->groupBy('start_month')
            ->orderBy('start_date');
        $monthsInYear = $monthsInYearQuery->get();

        $returnableMonths = [];
        foreach ($monthsInYear as $month) {
            $returnableMonths[] = \DateTime::createFromFormat('!m', $month->start_month);
        }

        return $returnableMonths;

    }

    public function getHtmlDescription()
    {
        if (trim($this->description) == '') {
            return '';
        }

        return '<p>' . nl2br($this->description) . '</p>';
    }

    public function getAddress()
    {

    }

    public function getJson()
    {
        $startDate = new \DateTime($this->start_date);
        $endDate = new \DateTime($this->end_date);
        $json = [
            '@context' => 'http://schema.org',
            '@type' => 'Event',
            '@name' => $this->summary,
            'startDate' => $startDate->format('c'),
            'endDate' => $endDate->format('c'),
            'url' => '/' . $this->slug,
            'location' => [
                '@type' => 'Place',
                'name' => $this->location,
                'address' => $this->getAddress()
            ]
        ];
        return json_encode($json);
    }
}
